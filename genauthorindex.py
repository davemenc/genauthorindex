import sys
import os
from docx import Document
import string

def GetTOC(TOCFname):
	tocin = open(TOCFname,"r")
	TOCList = {}
	for line in tocin:
		linefields = line.strip().split("\t")
		if len(linefields)>1:
			Title=linefields[0].lower().strip()
			PageNo = linefields[1].strip()
			TOCList[Title]=PageNo
	return TOCList

def GetExampleList(ExampleListFname):
	ExampleList = {}
	ExObj = open(ExampleListFname,"r")
	for line in iter(ExObj):
		fields = line.strip().split("\t")
		DocFileName = fields[0].strip()
		PixFileName = fields[1].strip()
		Title = fields[2].lower().strip()
		Category = fields[3].strip()
		Index = fields[4].strip()
		FirstPageChar = fields[5].strip()
		ExampleList[Title]=DocFileName
	return ExampleList
		
def GenTOCDict(TOCFname,ExampleList):
	TOCList = GetTOC(TOCFname)
	FilePage ={}
	for k in TOCList.keys():
		if k in ExampleList:
			FilePage[ExampleList[k]]=int(TOCList[k])
	return FilePage

def Help(Name):
	print "Usage: "+Name+" authorsbyfiles   toc examplelist authorindex.txt" 
	print Name+" is a program to generate an index file for the book generator."
	print "Where:"
	print "   authorsbyfiles: A tab delim list relating the example files to their authors."
	print "   examplelist: The list of examples, tab delim with text,chart, title, category & order for each example."
	print "   toc: The table of contents file, a tab delim list of section titles and page numbers."
	print "   authorindex: The name of the file you want the final author index to be in."

def ReadAuthorFileList(fname):
	#print "ReadWordList",fname
	List={}
	f = open(fname,"r")
	for line in iter(f):
		if len(line)<10:
			continue
		fields = line.strip().split("\t")
		Filename = fields[0]
		Author = fields[1]
		List[Filename] = Author
	return(List)

EXPECTED_ARGS=5
if len(sys.argv)==EXPECTED_ARGS:
	authorsbyfiles = sys.argv[1]
	TOCFname =sys.argv[2]
	ExampleListFname = sys.argv[3]
	AuthorIndexFname = sys.argv[4]
else:
	print "Wrong number of arguments; expected", EXPECTED_ARGS,"Got ",len(sys.argv)
	Help(sys.argv[0])
	exit()

ExampleList = GetExampleList(ExampleListFname) # get a list of examples (title and fnames)
TocDict=GenTOCDict(TOCFname,ExampleList) # get from that, a list of fnames and page #s
AuthorFileList= ReadAuthorFileList(authorsbyfiles)
OutFile = open(AuthorIndexFname,"w")

#print "TocDict",TocDict
#print "AuthorFileList",AuthorFileList

IndexList = {}
for filename in TocDict.keys():
	Author = AuthorFileList[filename]
	Page = TocDict[filename]
	if Author in IndexList:
		IndexList[Author].append(Page)
	else:
		IndexList[Author]=[Page]

print IndexList
FoundAuthorList = IndexList.keys()
FoundAuthorList.sort()
for Author in FoundAuthorList:
	pagelist=""
	IndexList[Author].sort()
	print Author+"\t",IndexList[Author]
	OutFile.write(Author+"\t")
	LastPage = -1
	FirstPageNo = True
	for pageno in IndexList[Author]:
		if pageno==LastPage:
			continue
		else:
			LastPage = pageno
		if not FirstPageNo:
			OutFile.write(", ")
		else:
			FirstPageNo=False
		OutFile.write(str(pageno))
	OutFile.write("\n")
OutFile.close()

exit()
IndexedWordList = {}
for fname in TocDict.keys():
	InHeader = True
	if not os.path.isfile(Path+fname):
		print "~~~~~~~ file "+fname+" does not exist."
		continue
	BasePage = TocDict[fname]
	#print fname, BasePage
	PageCount = 0
	ParaCount = 0
	CharacterCount = 0
	document = Document(Path+fname)
	for para in document.paragraphs:
		ParaCount+=1
		filtered_string = ''.join(filter(lambda x:x in string.printable, para.text))
		# figure out if we're in the header still
		if filtered_string.find(HeaderKey)!=-1: #after this we're out of the header
			InHeader=False
			continue
		if InHeader: #if we are in the header, skip this paragraph
			continue
		for word in WordList:
			index = filtered_string.find(word)
			
			if index!=-1:# We found a word!
				BumpPage = int((index+CharacterCount-CharactersOnFirstPage)/CharactersPerPage+.99999) # subtract first page, divide by per page and round up to next int
				ThisPage=BumpPage+BasePage+1
				if word in IndexedWordList:
					IndexedWordList[word].append(ThisPage)
				else:
					IndexedWordList[word]=[ThisPage]


				mystring = filtered_string.replace("\n"," ")
				start = index-20
				if start<0:
					start=0
				end = index+30
				if end>len(	mystring)-1:
					end=len(mystring)-1
				#print "~"+word+"|",ThisPage, "|"+mystring[start:end].strip()+"|",fname,CharacterCount+index, len(mystring),start,end
		CharacterCount+=len(filtered_string)
#print IndexedWordList
FoundWordList = IndexedWordList.keys()
FoundWordList.sort()
for word in FoundWordList:
	pagelist=""
	IndexedWordList[word].sort()
	#print word+"\t",IndexedWordList[word]
	wordname=word[0:1].upper()+word[1:]
	OutFile.write(wordname+"\t")
	LastPage = -1
	FirstPageNo = True
	for pageno in IndexedWordList[word]:
		if pageno==LastPage:
			continue
		else:
			LastPage = pageno
		if not FirstPageNo:
			OutFile.write(", ")
		else:
			FirstPageNo=False
		OutFile.write(str(pageno))
	OutFile.write("\n")
OutFile.close()
